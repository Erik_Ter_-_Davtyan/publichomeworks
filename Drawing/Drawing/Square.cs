﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;
using System.Threading;

namespace DrawingObjects
{
    class Square
    {
        public int SideLength { get; set; }
        public ConsoleColor Color { get; set; }
        public bool IsRandomColored { get; set; }
        public char Symbol { get; set; }
        public void Draw()
        {
            if (IsRandomColored == true)
            {
                Random rd = new Random();
                for (int i = 0; i < SideLength; i++)
                {
                    for (int j = 0; j < SideLength; j++)
                    {
                        Thread.Sleep(200);
                        ForegroundColor = (ConsoleColor)rd.Next(1,16);
                        Write(Symbol + " ");
                    }
                    WriteLine();
                }
            }
            else
            {
                for (int i = 0; i < SideLength; i++)
                {
                    for (int j = 0; j < SideLength; j++)
                    {
                        Thread.Sleep(200);
                        ForegroundColor = Color;
                        Write(Symbol + " ");
                    }
                    WriteLine();
                }
            }
        }
    }
}
