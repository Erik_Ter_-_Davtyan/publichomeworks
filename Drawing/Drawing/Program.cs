﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace DrawingObjects
{
    class Program
    {
        static void Main(string[] args)
        {
            Triangle triangle = new Triangle
            {
                Heigth = 5,
                Symbol = '*',
                IsRandomColored = true
            };
            DrawTriangle(triangle);
            ReadLine();

        }
        static public void DrawTriangle(Triangle trinagle)
        {                                        
            trinagle.Draw();
            ResetColor();
        }
    }
}
