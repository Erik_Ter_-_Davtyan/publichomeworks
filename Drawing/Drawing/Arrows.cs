﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace DrawingObjects
{
    class Arrows:Line
    {
     
        public bool IsDouble { get; set; }
        enum Direction
        {
            None,
            Up,
            Down,
            Left,
            Right
        }
        public void DrawArrow()
        {
            if (IsPerpendicular == true)
            {
                if (IsDouble == true)
                {
                    if (IsColorfull == true)
                    {
                        Random rd = new Random();
                        WriteLine('^');
                        for (int i = 0; i < Length; i++)
                        {
                            ForegroundColor = (ConsoleColor)rd.Next(1, 16);
                            WriteLine(Symbol);
                        }
                        WriteLine('^');
                    }
                    else
                    {
                        WriteLine('^');
                        for (int i = 0; i < Length; i++)
                        {
                            ForegroundColor = Color;
                            WriteLine(Symbol);
                        }
                        WriteLine('^');
                    }
                }
                
            }

        }

    }
}
