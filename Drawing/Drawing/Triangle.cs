﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace DrawingObjects
{
    class Triangle
    {
        public int Heigth { get; set; }
        public char Symbol { get; set; }
        public bool IsRandomColored { get; set; }
        public ConsoleColor Color { get; set; }
        public void Draw()
        {
            if (IsRandomColored == true)
            {
                Random rd = new Random();

                for (int i = 0; i < Heigth; i++)
                {
                    for (int k = 0; k <= i; k++)
                    {
                        ForegroundColor = (ConsoleColor)rd.Next(1,16);
                        Write(Symbol);
                    }
                    WriteLine();
                }
            }
            else
            {
                for (int i = 0; i < Heigth; i++)
                {
                    for (int k = 0; k <= i; k++)
                    {
                        ForegroundColor = (ConsoleColor)Color;
                        Write(Symbol);
                    }
                    WriteLine();
                }

            }
            ResetColor();
        }

    }
}
