﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace DrawingObjects
{
    class Rectangle:Square
    {
        public int Heigth { get; set; }
        public int Width { get; set; }
        public void DrawRectangle()
        {
            if (IsRandomColored == true)
            {
                Random rd = new Random();
                for (int i = 0; i < Width;i++)
                {
                    for(int j = 0; j < Heigth; j++)
                    {
                        ForegroundColor = (ConsoleColor)rd.Next(1,16);
                        Write(Symbol);
                    }
                    WriteLine(); 
                }
            }
            else
            {
                for (int i = 0; i < Width; i++)
                {
                    for (int j = 0; j < Heigth; j++)
                    {
                        ForegroundColor = Color;
                        Write(Symbol);
                    }
                    WriteLine();
                }
            }
            ResetColor();
        }
    }
}
