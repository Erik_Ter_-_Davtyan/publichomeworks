﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace DrawingObjects
{
    class Line
    {
        public int Length { get; set; }
        public ConsoleColor Color { get; set; }
        public bool IsColorfull { get; set; }
        public bool IsPerpendicular { get; set; }
        public char Symbol { get; set; }
        public void Draw()
        {
            if (IsPerpendicular == true)
            {

                if (IsColorfull == true)
                {
                    Random rd = new Random();
                    for (int i = 0; i < Length;i++ )
                    {
                        ForegroundColor = (ConsoleColor)rd.Next(1,16);
                        WriteLine(Symbol);
                    }
                }
                else
                {
                    for (int i = 0; i < Length; i++)
                    {
                        ForegroundColor = Color;
                        WriteLine(Symbol);
                    }
                }
            }
            else
            {
                if (IsColorfull == true)
                {
                    Random rd = new Random();
                    for (int i = 0; i < Length; i++)
                    {
                        ForegroundColor = (ConsoleColor)rd.Next(1, 16);
                        Write(Symbol);
                    }
                }
                else
                {
                    for (int i = 0; i < Length; i++)
                    {
                        ForegroundColor = Color;
                        Write(Symbol);
                    }
                }
            }
        }
    }
}
